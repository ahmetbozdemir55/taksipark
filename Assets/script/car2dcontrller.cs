﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using DG.Tweening;
public class car2dcontrller : MonoBehaviour
{

	Rigidbody2D rb;
	
	[SerializeField]
	float accelerationPower = 5f;
	[SerializeField]
	float steeringPower = 5f;
	float steeringAmount, speed, direction;
	public GameObject Crash;
	public GameObject SuccesSprite;
	public GameObject Taxi1;
	public AudioSource CrashSound;

	// Use this for initialization
	void Start()
	{
		Taxi1.gameObject.SetActive(false);
		rb = GetComponent<Rigidbody2D>();
		SuccesAnimasyon();
    }
	public void Update()
	{
		
	}
	// Update is called once per frame
	void FixedUpdate()
	{
		steeringAmount = -Input.GetAxis("Horizontal");
		speed = Input.GetAxis("Vertical") * accelerationPower;
		direction = Mathf.Sign(Vector2.Dot(rb.velocity, rb.GetRelativeVector(Vector2.right)));
		rb.rotation += steeringAmount * steeringPower * rb.velocity.magnitude * direction;
		rb.AddRelativeForce(Vector2.right * speed);
		rb.AddRelativeForce(Vector2.up * rb.velocity.magnitude * steeringAmount / 2);

		float Xposition = Mathf.Clamp(transform.position.x, -7, 5.7f);
		float Yposition = Mathf.Clamp(transform.position.y, -5, 3.9f);
		transform.position = new Vector3(Xposition, Yposition, transform.position.z);

	}
	public void crashanimasyon ()
    {
		Crash.gameObject.transform.DOScale(new Vector3(0.4f, 0.4f, 0.4f), 0.7f).OnComplete(CrashObjeColes);
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
		if (collision.gameObject.tag != "Player")
		{
			CrashSound.Play();
			crashanimasyon();
		}
	}
    public void CrashObjeColes()
    {
		Crash.gameObject.transform.localScale = new Vector3(0, 0, 0);

	}
	public void SuccesAnimasyon()
    {
		SuccesSprite.gameObject.SetActive(true);
		SuccesSprite.gameObject.transform.DOScale(new Vector3(0.68f, 0.85f, 0.85f), 2f).OnComplete(SuccesObjeColes);
	}
	public void SuccesObjeColes()
	{
		SuccesSprite.gameObject.transform.DOScale(new Vector3(0f, 0f, 0f), 2f).OnComplete(SuccesAnimasyon);
	}
}
